import React,{createContext,useState} from 'react';

export type GlobalContent = {
    state: number
    setState:(c:number) => void
  }

const Context = createContext<GlobalContent>({state:0,setState: () => {}});
const GlobalState:React.FC = (props) => {
    const [state,setState] =useState(0);
    return (
        <React.Fragment>
            <Context.Provider value={{state,setState}}>
              {props.children}
            </Context.Provider>
        </React.Fragment>
    );
};

export default GlobalState;
export {Context};