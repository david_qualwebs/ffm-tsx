import React from 'react';
import {Switch,Route,Redirect} from "react-router-dom";
import HomePage from "../Components/Static/HomePage";


const Routes:React.FC = () => {
    return (
        <React.Fragment>
            <Switch>
             <Route exact path="/" component={HomePage}/>
             <Redirect to="/"/>
            </Switch>
        </React.Fragment>
    );
};

export default Routes;
