import React from 'react';
import "./assets/css/home.css";
import { BrowserRouter } from "react-router-dom";
import Layout from "./Components/Layout/Layout";
import Routes from "./Routes/Routes";
import GlobalState from "./GlobalState/GlobalState"

function App() {
   return (
      <React.Fragment>
         <GlobalState>
            <BrowserRouter>
               <Layout>
                  <Routes/>
               </Layout>
            </BrowserRouter>
         </GlobalState>
      </React.Fragment>
   );
}

export default App;
