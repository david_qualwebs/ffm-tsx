import React from 'react';

const Header:React.FC = () => {
    return (
        <React.Fragment>
            <div className="header-nav d-none d-md-none d-lg-block d-xl-block">
                <div className="container-fluid container-width">
                    <div className="row">
                        <div className="col-12 col-md-8 col-lg-12 col-xl-12">
                            <nav className="navbar navbar-expand-lg">
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="fa fa-bars"></span>
                                </button>
                                <div className="collapse navbar-collapse" id="navbarNav">
                                    <ul className="navbar-nav menu-details">
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Wholesale & Bulk</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Fresh Kitchen & catering</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link" href="#">Location & hours</a>
                                        </li>
                                    </ul>

                                    <ul className="navbar-nav menu-details right-menu ml-auto">

                                        <li className="nav-item">
                                            <a className="nav-link color-red" href="#"><i className="fa fa-heart"></i>Featured Favorites</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link color-blue" href="#"><i className="fa fa-line-chart" aria-hidden="true"></i>Best
                                              Seller</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Header;
