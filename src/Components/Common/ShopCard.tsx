import React from 'react';

interface Props{
    image:string
}

const ShopCard:React.FC<Props> = (props) => {
    return (
        <React.Fragment>
            <div className="shop-border food-details" data-aos="zoom-in" data-aos-easing="linear">
                <div><img src={props.image} alt="Not Found" /></div>
                <div className="shop-inner-details">
                    <p>Fresho Mixed Chicken Pieces Boneless, Antibiotic (2lb)</p>
                    <div className="row">
                        <div className="col-6 col-md-5 col-lg-5 col-xl-8 food-col">
                            <div className="food-price">
                                <p>$16.00</p>
                            </div>
                        </div>

                        <div className="col-6 col-md-7 col-lg-7 col-xl-4 cart-col">
                            <div className="buttons">
                                <button className="cart-button cart1">
                                    <span className="added">
                                        <div className="num-block skin-5">
                                            <div className="num-in">
                                                <span className="minus decrement dis" onClick={()=>{}}> - </span>
                                                <input type="text" className="in-num" value="1" name="qty" />
                                                <span className="plus increment" onClick={()=>{}}>+</span>
                                            </div>
                                        </div>
                                    </span>
                                    <span className="fa fa-shopping-basket add-to-cart"></span> 
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default ShopCard;
