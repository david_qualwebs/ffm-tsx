import React from 'react';

interface Props{
    image:string
}

const Card:React.FC<Props> = (props) => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                <div className="fresh-food-details" data-aos="zoom-in" data-aos-easing="linear">
                    <div>
                        <img src={props.image} className="img-fluid" alt="Not Found" />
                    </div>
                    <h5>Absolutely Fresh Chicken</h5>
                    <p>100% Natural & Chemical Free</p>
                    <button className="btn btn-order">Order now</button>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Card;
