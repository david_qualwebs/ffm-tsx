import React from 'react';
import SideBar from "../Common/SideBar";
import Banner from "../Common/Banner";
import FirstSection from "../HomePageComponents/FirstSection";
import Featured from '../HomePageComponents/Featured';
import MostPopular from '../HomePageComponents/MostPopular';
import AbsolutelySection from '../HomePageComponents/AbsolutelySection';
import NewMeat from '../HomePageComponents/NewMeat';
import TopSelling from '../HomePageComponents/TopSelling';
import Recommended from '../HomePageComponents/Recommended';
import ShopSelection from "../HomePageComponents/ShopSelection";
import Subscribe from '../HomePageComponents/Subscribe';

const HomePage: React.FC = () => {
    return (
        <React.Fragment>
            <div className="container-fluid">
                <div className="row">
                    <SideBar />
                    <div className="col-12 col-md-10 col-lg-10 col-xl-9 column">
                       <Banner/> 
                       <FirstSection/> 
                       <Featured/>
                       <MostPopular/>
                       <AbsolutelySection/>
                       <NewMeat/>
                       <TopSelling/>
                       <AbsolutelySection/>
                       <Recommended/>
                    </div> 
                </div>
            </div>
            <div className="container-fluid">
                <div className="bg-white shop-section">
                    <div className="row"> 
                       <ShopSelection/>
                    </div>
                </div>
            </div>
            <Subscribe/>
        </React.Fragment>
    );
};

export default HomePage;
