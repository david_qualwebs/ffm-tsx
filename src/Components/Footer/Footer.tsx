import React from 'react';

const Footer:React.FC = () => {
    return (
        <React.Fragment>
            <div className="footer" data-aos="zoom-in" data-aos-easing="linear">
                <div className="container-fluid">

                    <div className="row">
                        <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                            <div className="footer-content">
                                <h5>Fresh Meat</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor, luctus eu mauris, risus sagittis id
                                   aliquam vitae. Arcu ullamcorper et diam felis, dui augue volutpat. Molestie natoque a vestibulum
                                   turpis tincidunt imperdiet enim. Aliquam congue lacus diam suscipit enim pulvinar hendrerit interdum.
                                </p>
                            </div>
                        </div>

                        <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                            <div className="footer-content alignment">
                                <h5>Recent Posts</h5>
                                <ul>
                                    <li><a href="#">Buy fresh meat tomahawks in Houstan</a></li>
                                    <li><a href="#">Buy fresh meat tomahawks in Houstan</a></li>
                                    <li><a href="#">Buy fresh meat tomahawks in Houstan</a></li>
                                    <li><a href="#">Buy fresh meat tomahawks in Houstan</a></li>
                                    <li><a href="#">Buy fresh meat tomahawks in Houstan</a></li>
                                </ul>
                            </div>
                        </div>

                        <div className="col-12 col-md-4 col-lg-4 col-xl-4">
                            <div className="footer-content">
                                <h5>Contact Us</h5>
                                <div className="contact-details">
                                    <div className="row">
                                        <div className="col-1 col-md-2 col-lg-2 col-xl-1">
                                            <i className="fa fa-clock-o" aria-hidden="true"></i>
                                        </div>
                                        <div className="col-11 col-md-10 col-lg-10 col-xl-10">
                                            <div className="hours-details">
                                                <h6>Hours of Operations</h6>
                                                <p>Mon - Sat: 7am - 8pm</p>
                                                <p>Mon - Sat: 7am - 8pm</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-1 col-md-2 col-lg-2 col-xl-1">
                                            <i className="fa fa-map-marker" aria-hidden="true"></i>
                                        </div>
                                        <div className="col-11 col-md-10 col-lg-10 col-xl-10">
                                            <div className="hours-details">
                                                <h6>3897 Archwood Avenue, Mountain View,
                                                    Wyoming, 82939
                                                </h6>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-1 col-md-2 col-lg-2 col-xl-1">
                                            <i className="fa fa-phone"></i>
                                        </div>
                                        <div className="col-11 col-md-10 col-lg-10 col-xl-10">
                                            <div className="hours-details">
                                                <h6>+1 1234567890</h6>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-1 col-md-2 col-lg-2 col-xl-1">
                                            <i className="fa fa-envelope" aria-hidden="true"></i>
                                        </div>
                                        <div className="col-11 col-md-10 col-lg-10 col-xl-10">
                                            <div className="hours-details">
                                                <h6>contact@freshmeat.com</h6>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="bottom-footer">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 col-md-5 col-lg-6 col-xl-6 copyright-details">
                            <div>
                                <p className="copyright-content">Content copyright 2020. Farmer's Fresh Meat. All rights reserved.</p>
                            </div>
                        </div>
                        <div className="col-12 col-md-7 col-lg-6 col-xl-6">
                            <div className="footer-right">
                                <ul className="list-group list-group-horizontal">
                                    <li>
                                        <a href="#">SITEMAP</a>
                                    </li>
                                    <li>
                                        <a href="#">TERMS &amp; CONDITIONS</a>
                                    </li>
                                    <li>
                                        <a href="#">PRIVACY POLICY</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </React.Fragment>
    );
};

export default Footer;
