import React from 'react';
import { ShopData } from "./Data";
import ShopCard from "../Common/ShopCard";


const ShopSelection = () => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h5>Shop Selection</h5>

                    {/**********Food Details************/}

                    {
                        ShopData.map((val, index) => {
                            return (
                                <ShopCard
                                    key={index}
                                    image={val.image}
                                />
                            )
                        })
                    }
                    {/**********Food Details************/}
                </div>
                <div className="feature-details m-0"><a href="#">See more </a></div>
            </div>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h5>Frequent Bought</h5>
                    {/**********Food Details************/}

                    {
                        ShopData.map((val, index) => {
                            return (
                                <ShopCard
                                    key={index}
                                    image={val.image}
                                />
                            )
                        })
                    }
                    {/**********Food Details************/}
                </div>
                <div className="feature-details m-0"><a href="#">See more </a></div>
            </div>
            <div className="col-12 col-md-6 col-lg-4 col-xl-4">
                <div className="shop-details">
                    <h5>Delicious Deals</h5>
                    {
                        ShopData.map((val, index) => {
                            return (
                                <ShopCard
                                    key={index}
                                    image={val.image}
                                />
                            )
                        })
                    }
                </div>
                <div className="feature-details m-0"><a href="#">See more </a></div>
            </div>

        </React.Fragment>
    );
};

export default ShopSelection;
