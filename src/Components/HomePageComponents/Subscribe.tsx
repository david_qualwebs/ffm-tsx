import React from 'react';

const Subscribe = () => {
    return (
        <React.Fragment>
            <div className="container-fluid">
                <div className="subscribe-info" data-aos="zoom-in" data-aos-easing="linear">
                    <div className="row">
                        <div className="col-12 col-md-10 col-lg-10 col-xl-8 mx-auto">
                            <div className="subscribe-details text-center">
                                <h5>Subscribe to For Special Offer</h5>
                                <p>‘Carrier charge may apply’</p>
                                <form>
                                    <div className="food-details subscribe-display">
                                        <div className="form-group">
                                            <input type="text" className="form-control" placeholder="Your name" />
                                        </div>
                                        <div className="form-group">
                                            <input type="email" className="form-control" placeholder="Your email" />
                                        </div>
                                        <div className="form-group">
                                            <input type="tel" className="form-control" placeholder="Your phone" />
                                        </div>
                                    </div>
                                    <div>
                                        <button className="btn btn-order px-5">Subscribe now</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </React.Fragment>
    );
};

export default Subscribe;
