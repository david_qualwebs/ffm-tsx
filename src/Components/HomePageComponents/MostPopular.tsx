import React from 'react';
import Slider from "../Common/Slider";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import {DataSlider} from "./Data";


const MostPopular = () => {
  
    return (
        <React.Fragment>
           <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div className="food-details feature-details"> 
                        <div>
                            <h5>Most Popular at Meat & Grocery</h5>
                        </div>
                        <div><a href="#">See more </a></div>
                    </div>
                </div>
                <div className="col-12 col-md-12 col-lg-12">
                    <OwlCarousel className="owl-theme owl-carousel" loop margin={10} nav items={5}>

                        {

                            DataSlider.map((val, index) => {
                                return (
                                    <Slider
                                        key={index}
                                        image={val.image}
                                    />
                                )
                            })
                        }

                    </OwlCarousel>
                </div>
            </div>

        </React.Fragment>
    );
};

export default MostPopular;
