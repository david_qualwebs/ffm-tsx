import React from 'react';
import Data from "./Data";
import FoodCard from "../Common/FoodCard";

const FirstSection: React.FC = () => {
    return (
        <React.Fragment>
            <div className="row display-details">
                {
                    Data.map((val, index) => {
                        return (
                            <FoodCard
                                key={index}
                                image={val.image}
                            />
                        )
                    })
                }
            </div>
        </React.Fragment>
    );
};

export default FirstSection;
