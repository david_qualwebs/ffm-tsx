import React from 'react';
import Card from '../Common/Card';
import { CardData } from "./Data";


const AbsolutelySection: React.FC = () => {
    return (
        <React.Fragment>
            <div className="row">
                {
                    CardData.map((val, index) => {
                        return (
                            <Card
                                key={index}
                                image={val.image}
                            />
                        )
                    })
                }
            </div>
        </React.Fragment>
    );
};

export default AbsolutelySection;
